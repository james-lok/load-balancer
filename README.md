# Using bash script to install website
## Prerequisites
* You have cloned this repository using:

```git clone git@bitbucket.org:garthj98/haproxy-loadbalancer.git```

* Have access to cloud computers and their public ip, at least one for loadbalancing and one for your webserver

## Instructions

### 1. Make sure you are located in the directory of this repository

``` $ cd path/to/repo```

### 2. Type out the following command with your arguments
```bash
# Run the bashscipt with the arguments of the ssh key, the ip of your load balancing computer, and your ips of your webservers seperated by a space
$ proxy_install.sh <path/to/key> <loadBalancerIp> <webServerIp1> ... <webServerIpn>
```