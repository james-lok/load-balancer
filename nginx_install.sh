# Get onput from arguments of IP to use in the script
# The first argument passed to the script hsould b IP 

# extra make script second atgumetn become the key name or path 
# extra make scipt argument become user to login to machine

if [ $# != 3 ]
then
    echo "Expected 3 argument, $# was given instead"
    exit 1
fi

key=$1
hostname=$2

if ! output=$(ssh -o StrictHostKeyChecking=no -i $key ubuntu@$hostname sudo nginx -v)
then
    echo "nginx could not be found"
    ssh -o StrictHostKeyChecking=no -i $key ubuntu@$hostname '
    sudo apt update
    sudo apt install nginx -y
    '
fi

scp -i  $key -r james_website ubuntu@$hostname:

serverName="Server$3"
ssh -o StrictHostKeyChecking=no -i $key ubuntu@$hostname << EOF
sudo sed 's/James/$serverName/' ~/james_website/template.html > ~/james_website/index.html
sudo mv ~/james_website/index.html /var/www/html/index.html
sudo service nginx restart
EOF
