if [ $# -lt 3 ]
then
    echo "Expected at least 3 argument, $# was given instead"
    exit 1
fi

key=$1
loadbalancer=$2

counter=1
configlines=""
for arg in $*
do
    if [ $counter -gt 2 ]       
    then 
        serverNo=$((counter-2))
        # Gets the private ip of the webservers and generates config lines for each
        private_ip=$(ssh -i $key ubuntu@$arg "hostname -I" 2>&1| xargs)
        configlines+="server node$serverNo $private_ip:80 check
    "
        # Launches website on the webserver
        ./nginx_install.sh $key $arg $serverNo
    fi
    ((counter++))
done
james

# installs haproxy on our load balancer if it isn't already installed
if ! output=$(ssh -o StrictHostKeyChecking=no -i $key ubuntu@$loadbalancer sudo haproxy -v)
then
    echo "haproxy could not be found. will install now"
    ssh -o StrictHostKeyChecking=no -i $key ubuntu@$loadbalancer '
    sudo apt update
    sudo apt install haproxy -y
    '
fi

# Writes config file including our backend ips to load balancing computer
ssh -o StrictHostKeyChecking=no -i $key ubuntu@$loadbalancer  << EOF
sudo bash -c 'echo "
defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

frontend haproxynode
    bind *:80
    mode http
    default_backend backendnodes

backend backendnodes
    balance roundrobin
    option forwardfor
    $configlines
" > /etc/haproxy/haproxy.cfg'
sudo systemctl restart haproxy
EOF

open http://$loadbalancer
# 34.243.132.123
# 172.31.9.245
# 18.203.65.229
# 172.31.2.45
# 54.170.89.128